from abc import ABC, abstractclassmethod

class Animal(ABC):
    @abstractclassmethod
    def eat(self, food):
        pass

    @abstractclassmethod
    def make_sound(self):
        pass

class Dog(Animal):
    def __init__(self, name, breed, age):
        self._name = name
        self._breed = breed
        self._age = age

    def set_name(self, name):
        self._name = name

    def get_name(self):
        return self._name
    
    def set_breed(self, breed):
        self._breed = breed

    def get_breed(self):
        return self._breed
    
    def set_age(self, age):
        self._age = age

    def get_age(self):
        return self._age

    def call(self):
        print(f"Here, {self.get_name()}!")

    def eat(self, food):
        print(f"HEE HEE HEE' {food}, master!!! hungry me!")

    def make_sound(self):
        print("RRUFFF!! AHHH!!! AHHH!!")
    
    def show_details(self):
        print(f"I am {self.get_name()}, a kind of {self.get_breed()} dog. I am {self.get_age()} years old!")


class Cat(Animal):
    def __init__(self, name, breed, age):
        self._name = name
        self._breed = breed
        self._age = age

    def set_name(self, name):
        self._name = name

    def get_name(self):
        return self._name
    
    def set_breed(self, breed):
        self._breed = breed

    def get_breed(self):
        return self._breed
    
    def set_age(self, age):
        self._age = age

    def get_age(self):
        return self._age
    
    def call(self):
        print(f"{self.get_name()}, fast!")

    def eat(self, food):
        print(f"Slave! Feed me {food}")

    def make_sound(self):
        print("Mnyaakk!")

    def show_details(self):
        print(f"I am {self.get_name()}, a kind of {self.get_breed()} cat. I am {self.get_age()} years old!")


dog = Dog("Parkley", "Beagle", 10)
dog.show_details()
dog.eat("Liver Spread")
dog.make_sound()
dog.call()

print("")

cat = Cat("Mingming", "Puspin", 5)
cat.show_details()
cat.eat("Turkey")
cat.make_sound()
cat.call()